
const kochiButton = document.getElementById('kochiButton')
const tvmButton = document.getElementById('tvmButton')
const temperatureSpan =document.getElementById('temperature')
const citySpan =document.getElementById('city')

kochiButton.addEventListener('click',()=>{getTemperature(8.5241,76.9366)})
tvmButton.addEventListener('click',()=> {getTemperature(9.9312,76.2673)})


function getTemperature(latitude,longitude){
    fetch(`https://api.open-meteo.com/v1/forecast?latitude= ${latitude} &longitude= ${longitude} &current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
.then( (response) => response.json())
.then((data) => {
    const temperature = data.current.temperature_2m
    temperatureSpan.innerHTML= `${temperature}&deg; C`
    
})
.catch( error => console.log(error)) 
}

